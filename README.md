## Multiple Choice Questions

- **a.** Which command is used to create a new branch in GitLab?
  - 1. `git add`
  - 2. `git branch`
  - 3. `git commit`
  - 4. `git checkout`

- **b.** What does CI/CD stand for in GitLab parlance?
  - 1. Continuous Integration/Continuous Deployment
  - 2. Centralized Integration/Continuous Deployment
  - 3. Continuous Integration/Continuous Development
  - 4. Continuous Inspection/Continuous Deployment

- **c.** What is the purpose of GitLab's Merge Requests?
  - 1. To request a code review
  - 2. To merge multiple branches
  - 3. To create a new branch
  - 4. To revert commits

- **d.** Which GitLab feature allows you to track issues and bugs in your projects?
  - 1. Merge Requests
  - 2. Issues Boards
  - 3. Wiki Pages
  - 4. Labels

- **e.** What is the purpose of using tags in GitLab, and how can they be created?
  - 1. To mark important points in history. Created via the GitLab UI or git tag <tagname> on the command line.
  - 2. To create new branches. Only created through the GitLab UI.
  - 3. To track merge requests. Automatically created on merge into the default branch.
  - 4. To assign tasks to team members. Created through the "Issues" section.

- **f.** How do we check the history of git commits using a single git command?
  - 1. `git status`
  - 2. `git diff`
  - 3. `git log --oneline`
  - 4. `git checkout`

- **g.** What is the difference between `git pull` and `git clone`
  - 1. 'git pull' Update local branch with remote changes, 'git clone' Copy entire remote repository to local machine.
  - 2. 'git pull' After cloning, to update local branch, 'git clone' Initial copying of repository.
  - 3. 'git pull' Merges remote changes into current branch, 'git clone' Creates a new local repository.
  - 4. 'git pull' Ongoing updates in development, 'git clone' Initial setup of project.

## True or False

- **a.** GitLab is a version control system used primarily for managing source code. (True/False)

- **b.** GitLab's Continuous Integration (CI) feature automates the process of running tests whenever new code is pushed to a repository. (True/False)

- **c.** GitLab's Pages feature allows users to host static websites directly from their GitLab repository. (True/False)

- **d.** GitLab provides built-in support for both centralized and distributed version control workflows. (True/False)

## Short Answer Questions

- **a.** Explain the difference between GitLab's "Merge Requests" and "Issues".

- **b.** Describe the purpose of GitLab Runners in the context of Continuous Integration/Continuous Deployment.

- **c.** How can you revert a commit in GitLab?

- **d.** Briefly explain what a GitLab "Pipeline" is and how it works.

## Practical Tasks

- **a.** Create a new branch named "feature-branch" in your GitLab repository.

- **b.** Write a `.gitlab-ci.yml` configuration file that defines a basic CI/CD pipeline consisting of two stages: "build" and "test". The "build" stage should echo "Building..." and the "test" stage should echo "Testing...".

- **c.** Add a stage called "Demo" to the pipeline which will echo the following text: "Demo stage of the pipeline". This stage should be triggered manually. Here's the [hint](https://docs.gitlab.com/ee/ci/jobs/job_control.html#rules-examples)

- **e.** Open a Merge Request for the "feature-branch" against the main branch of your repository.

- **f.** Add a label named "bug" to an existing issue in your GitLab project.
